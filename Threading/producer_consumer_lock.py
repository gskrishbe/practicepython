from threading import Lock
from loguru import logger
import os
import sys
import time
import concurrent.futures as cf
import random

logger.add(sys.stderr,
           format = "{time} {level} {message}",
           filter = 'thread_pool_executor',
           level = "INFO")

logger.add(os.path.join(os.getcwd(), "logs", "logs_{time:YYYY-MM-DD}.log"),
           format = "{time} {level} {message}",
           filter = "thread_pool_executor",
           level = "ERROR")

class Pipeline:

    def __init__(self):
        self.message = 0
        self.p_lock = Lock()
        self.c_lock = Lock()
        self.c_lock.acquire()

    def set_message(self, message, name):
        #logger.debug(f"{name} about to acquire set lock")
        self.p_lock.acquire()
        #logger.debug(f"{name} acquired the set lock")
        self.message = message
        #logger.debug(f"{name} about to release the getlock")
        self.c_lock.release()
        #logger.debug(f'{name} has released the get lock')

    def get_message(self, name):
        #logger.debug(f"{name} about to acquire get lock")
        self.c_lock.acquire()
        #logger.debug(f"{name} has acquired get lock")
        message = self.message
        #logger.debug(f"{name} about to release the set lock")
        self.p_lock.release()
        #logger.debug(f"{name} has released the set lock")
        return message

SENTINEL = object()

def producer(pipeline):
    for index in range(10):
        message = random.randint(1,101)
        logger.info(f"Producer got message {message}")
        pipeline.set_message(message, "Producer")

    pipeline.set_message(SENTINEL, "Producer")

def consumer(pipeline):
    message = 0
    while message is not SENTINEL:
        message = pipeline.get_message("Consumer")
        if message is not SENTINEL:
            logger.info(f"Consumer storing message {message}")

if __name__ == '__main__':
    pipeline = Pipeline()
    with cf.ThreadPoolExecutor(max_workers=2) as executor:
        for index in range(2):
            executor.submit(consumer, pipeline)
            executor.submit(producer, pipeline)
