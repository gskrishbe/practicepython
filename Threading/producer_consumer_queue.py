from threading import Lock, Event
from loguru import logger
import os
import sys
import time
import concurrent.futures as cf
import random
import queue

logger.add(sys.stderr,
           format = "{time} {level} {message}",
           filter = 'thread_pool_executor',
           level = "INFO")

logger.add(os.path.join(os.getcwd(), "logs", "logs_{time:YYYY-MM-DD}.log"),
           format = "{time} {level} {message}",
           filter = "thread_pool_executor",
           level = "ERROR")


def producer(queue, event):
    while not event.is_set():
        message = random.randint(1,101)
        logger.info(f"Producer got message {message}")
        queue.put(message)
    logger.info(f"producer received event. Exiting")

def consumer(queue,event):
    while not event.is_set() or not queue.empty():
        message = queue.get()
        logger.info(f'Consumer storing {message} and length of queue is {queue.qsize()}')
    logger.info("Consumer received event. Exiting")


if __name__ == '__main__':
    pipeline = queue.Queue(maxsize=10)
    event = Event()
    with cf.ThreadPoolExecutor(max_workers=2) as executor:
        executor.submit(producer, pipeline, event)
        executor.submit(consumer, pipeline, event)

        time.sleep(0.1)
        logger.info("Main: about to set event")
        event.set()