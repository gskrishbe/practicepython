import concurrent.futures as cf
from loguru import logger
import os
import sys
import time

logger.add(sys.stderr,
           format = "{time} {level} {message}",
           filter = 'thread_pool_executor',
           level = "INFO")

logger.add(os.path.join(os.getcwd(), "logs", "logs_{time:YYYY-MM-DD}.log"),
           format = "{time} {level} {message}",
           filter = "thread_pool_executor",
           level = "ERROR")

class FakeDatabase:

    def __init__(self):
        self.value = 0

    def update(self, name):
        logger.info(f"Starting to update by thread {name}")
        local = self.value
        local += 1
        time.sleep(0.5)
        self.value = local
        logger.info(f"Finished to update by thread{name}")

if __name__ == '__main__':
    database = FakeDatabase()
    logger.info(f"Testing started. initial value is {database.value}")
    with cf.ThreadPoolExecutor(max_workers=1) as executor:
        for i in range(1):
            executor.submit(database.update, i)
    logger.info(f"Testing finished. final value is {database.value}")
