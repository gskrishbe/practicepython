from threading import Lock
from loguru import logger
import os
import sys
import time
import concurrent.futures as cf

logger.add(sys.stderr,
           format = "{time} {level} {message}",
           filter = 'thread_pool_executor',
           level = "INFO")

logger.add(os.path.join(os.getcwd(), "logs", "logs_{time:YYYY-MM-DD}.log"),
           format = "{time} {level} {message}",
           filter = "thread_pool_executor",
           level = "ERROR")

class FakeDatabase:

    def __init__(self):
        self.value = 0
        self._lock = Lock()

    def update(self, name):
        logger.info(f"Starting to updated thread {name}")
        logger.debug(f"before lock acquired")
        with self._lock:
            logger.debug(f"Acquiring lock to hold the values")
            local_copy = self.value
            local_copy += 1
            self.value = local_copy
            time.sleep(0.1)
            logger.debug(f"About to release the lock")

        logger.debug("Lock has been released")
        logger.info(f"Finished updated thread {name} and value is {self.value}")


if __name__ == '__main__':

    database = FakeDatabase()
    logger.info(f"start Testing lock method value is {database.value}")

    with cf.ThreadPoolExecutor(max_workers=3) as executor:
        for index in range(3):
            executor.submit(database.update, index)

    logger.info(f"Finished Testing lock method final value {database.value}")