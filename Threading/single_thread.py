from loguru import logger
import sys
import os
from threading import Thread
import time

logger.add(sys.stderr,
           format = "{time} {level} {message}",
           filter = "single_thread",
           level = "INFO"
           )

logger.add(os.path.join(os.getcwd(), "logs", "logs_{time:YYYY-MM-DD}.log"),
           format = "{time} {level} {message}",
           filter = "single_thread",
           level = "ERROR"
           )

def thread_func(name):
    """
    Threading function
    @param name: string
    """
    logger.info(f"Starting thread {name}")
    time.sleep(10)
    logger.info(f"Finished thread {name}")

if __name__ == '__main__':
    logger.info("Before creating thread")
    thread = Thread(target=thread_func, args=(1,), daemon=True)
    logger.info("Before starting thread")
    thread.start()
    logger.info("Waiting for thread to finish")
    #Without join and daemon set to true will kill the thread function once the main gets killed
    #join function is to wait the thread process to get finished even if main function exits.
    thread.join()
    logger.info("All done")
