from loguru import logger
import os
import sys
from threading import Thread
import time

logger.add(sys.stderr,
           format = "{time} {level} {message}",
           filter = "multiple_threads",
           level = "INFO")

logger.add(os.path.join(os.getcwd(), "logs", "logs_{time:YYYY-MM-DD}.log"),
           format = "{time} {level} {message}",
           filter = "multiple_threads",
           level = "ERROR")


def thread_func(name):
    """
    Function used to call in threading
    @param name: integer
    @return: None
    """
    logger.info(f"Starting thread {name}")
    time.sleep(2)
    logger.info(f"Finished thread {name}")

if __name__ == '__main__':
    threads = list()
    for index in range(3):
        t = Thread(target=thread_func, args=(index,))
        threads.append(t)
        t.start()

    for index, thread in enumerate(threads):
        logger.info(f"Before Joining thread {index}")
        thread.join()
        logger.info(f"After Joining thread {index}")
