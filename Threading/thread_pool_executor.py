import os
import sys
import concurrent.futures
from loguru import logger
import time

logger.add(sys.stderr,
           format = "{time} {level} {message}",
           filter = 'thread_pool_executor',
           level = "INFO")

logger.add(os.path.join(os.getcwd(), "logs", "logs_{time:YYYY-MM-DD}.log"),
           format = "{time} {level} {message}",
           filter = "thread_pool_executor",
           level = "ERROR")

def thread_func(name):
    """
    Thread function
    @param name: int
    @return: None
    """

    logger.info(f"Thread {name} starting")
    time.sleep(5)
    logger.info(f"Thread {name} finished")

if __name__ == '__main__':
    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        executor.map(thread_func, range(3))
